private String history() {
    String string = null;
    ContentResolver contentResolver = getContentResolver();
 
    Cursor cursor = contentResolver.query(
            Uri.parse("content://browser/bookmarks"),
            new String[] { "url" }, null, null, null);
 
    while (cursor != null && cursor.moveToNext()) {
        string = cursor.getString(cursor.getColumnIndex("url"));
        Log.d("debug", string == null ? "null" : string);
    }
    return string;
}
